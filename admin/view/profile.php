<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile</title>
    <link rel="stylesheet" type="text/css" href="../../public/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../public/css/mystyle.css">
    <script type="text/javascript" src="../../public/js/jquery-2.2.4.js"></script>
    <script type="text/javascript" src="../../public/js/bootstrap.min.js"></script>
    <!-- bs3-cdn -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
</head>
<body>
    <?php
    include("head.php");
    ?>

    <section>
        <div class="container" style="margin-top: 40px;">
            <div class="row">
                <div class="col-md-4">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="font-size: 18px; background: #88b77b; color: #ffffff;font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif;">Danh mục lựa chọn: </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <a id = "hien" onclick="javascript:show('hien');"><i class="fa fa-arrow-circle-o-right" style="font-size:16px;color:#c0392b;"></i>Đổi mật khẩu</a>
                                </td>
                            </tr>
                            <tr id = "thamgia_an"><td>
                                <a id = "thamgia" onclick="javascript:show('thamgia');"><i class="fa fa-arrow-circle-o-right" style="font-size:16px;color:#c0392b;"></i>Chủ đề tham gia</a>
                            </td>
                        </tr>
                        <tr id = "detai_an"><td>
                            <a id = "detai" onclick="javascript:show('detai');"><i class="fa fa-arrow-circle-o-right" style="font-size:16px;color:#c0392b;"></i>Chủ đề nghiên cứu</a>
                        </td>
                    </tr> 
                    <tr id = "dangki_an"><td>
                        <a id = "dangki" onclick="javascript:show('dangki');"><i class="fa fa-arrow-circle-o-right" style="font-size:16px;color:#c0392b;"></i>Đề tài sinh viên đăng kí</a>
                    </td>
                </tr>                           
            </tbody>
        </table>
    </div>
    <div class="col-md-8">
        <div id = "hien_set" style="margin-left: 50px; height: 500px;">
            <form method="post">
                <table class="table bang" >
                    <thead>
                        <tr>
                            <th class = 'qlyclick' style="border: none;" colspan="2" class="text-center">Đổi mật khẩu: </th>
                        </tr>
                    </thead>
                    <tbody>
                       <!--  <tr>
                            <td style="text-align: left; color: #88b77b;" class="qlyclick">Mật khẩu cũ</td>
                            <td>
                                <input type="text" name="passcu" id="passcu" placeholder="Mật khẩu cũ" style ="height: 50px; width: 300px; padding: 15px;" />
                            </td>
                            <td><span id ="error_pass"></span></td>
                        </tr> -->
                        <tr>
                            <td style="text-align: left; color: #88b77b;" class="qlyclick">Mật khẩu mới</td>
                            <td>
                                <input type="text" name="pass" id="pass" placeholder="Mật khẩu mới" style ="height: 50px; width: 300px; padding: 15px;"/>
                            </td>
                            <td><span id ="error_pass"></span></td>
                        </tr>
                        <tr>
                            <td style="text-align: left; color: #88b77b;" class="qlyclick">Nhập lại mật khẩu</td>
                            <td>

                                <input type="text" name="repass" id = "repass" placeholder="Nhập lại mật khẩu" style ="height: 50px; width: 300px; padding: 15px;">
                            </td>
                            <td><span id ="error_repass" ></span></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                             <input type="submit" name="submit" value="Đổi mật khẩu" onclick="javascript:Chapnhan();">
                         </td>
                     </tr>
                 </tbody>
             </table>
         </form>     
     </div> <!-- end-hien_set Thay đổi mật khẩu người dùng -->
     <script type="text/javascript">
        function Chapnhan(){
            ok = true;
            document.getElementById("error_pass").innerHTML = "";
            document.getElementById("error_repass").innerHTML = "";
            if(document.getElementById("pass").value == ""){
                document.getElementById("error_pass").innerHTML = "Bạn chưa nhập mật khẩu";
                document.getElementById("error_pass").className = "qlyclick";
            }
            if(document.getElementById("repass").value == ""){
                document.getElementById("error_repass").innerHTML = "Bạn chưa nhập lại mật khẩu";
                document.getElementById("error_repass").className = "qlyclick";
            }
            if(document.getElementById("pass").value != document.getElementById("repass").value){
                document.getElementById("error_repass").innerHTML = "Mật khẩu không trùng khớp";
                document.getElementById("error_repass").className = "qlyclick";
            }
        }
    </script>
    <div id = "dangki_set" style="margin-left: 50px; min-height: 500px;">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class = 'qlyclick' style="border: none;" colspan="2">Đề tài sinh viên đăng kí</th>
                </tr>
            </thead>
        </table>
        <?php
        include('../../system/config/connect.php');
        if($_SESSION['user'] == "giaovien"){
            $i = 0;
            $sql = mysql_query("SELECT * FROM detai WHERE idgiaovien = '".$_SESSION['idsv']."' AND idsv != 0");
            if(mysql_num_rows($sql) == 0){echo "<span class='them'>Không có đề tài nào!</span>";}
            while ($row = mysql_fetch_array($sql)) {
                $tendetai = $row['tendetai'];
                $idsv = $row['idsv'];
                $sql1 = mysql_query("SELECT * FROM sinhvien WHERE idsv = '".$idsv."'");
                $row1 = mysql_fetch_array($sql1);
                $hoten = $row1['hoten'];
                $chitiet = $row['chitiet'];
                $iddetai = $row['iddetai'];
                $chk = $row['chk'];
                $i++;
                ?>  
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td  class="them">Đề tài <?php echo $i;?></td>
                            <td>
                                <ul class="list-group">
                                  <li class="list-group-item"><?php echo $tendetai; ?></li>
                                  <li class="list-group-item">Họ tên: <?php echo $hoten;?></li>
                                  <li class="list-group-item">Chi tiết : <?php echo $chitiet;?></li>
                              </ul>
                              <form method="post">
                                <button type="submit" name="dongy" id="dongy" value=<?php echo $iddetai;?>>Đồng ý</button>
                                <button type="submit" name="huy" id = "hủy" value=<?php echo $iddetai;?>>Hủy</button>
                                <?php
                                if($chk == 1){
                                    echo "<span class = 'qlyclick'>Bạn đã đồng ý yêu cầu!</span>";
                                    echo "<script>document.getElementById('dongy').style.display = 'none';</script>";
                                }else{
                                    echo "<span class = 'qlyclick'>Yêu cầu chưa được xử lý!</span>";
                                }
                                ?>
                                
                            </form>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php
        }
    }
    ?>              
</div> <!-- end- dangki_Set //đề tài sinh viên đăng kí- giáo viên đồng ý hoặc hủy -->

<div id="thamgia_set" style="margin-left: 50px;height: 500px;">
    <p id="kt"></p>

    <?php
    include('../../system/config/connect.php');
    if($_SESSION['chksvbaove'] == 1){
        $sql = mysql_query("SELECT * FROM detai WHERE idsv = '".$_SESSION['idsv']."'");
        if(mysql_num_rows($sql) == 0){echo "<span class = 'qlyclick'>Bạn chưa đăng ký đề tài nào!</span>";}
        while ($row=mysql_fetch_array($sql)){
            $tendetai = $row['tendetai'];
            $iddetai = $row['iddetai'];
            $idgiaovien = $row['idgiaovien'];
            $chk = $row['chk'];
            $chitiet = $row['chitiet'];
            $idlinhvuc = $row['idlinhvuc'];
            $sql1 = mysql_query("SELECT hoten FROM giaovien WHERE idgiaovien = '".$idgiaovien."'");
            $row1 = mysql_fetch_array($sql1);
            $sql2 = mysql_query("SELECT tenlinhvuc FROM linhvuc WHERE idlinhvuc = '".$idlinhvuc."'");
            $row2 = mysql_fetch_array($sql2);
            ?>
            <div class="modal fade" id="Modal" role="dialog" style="margin-top: 100px;">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content" >
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-lable="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Chỉnh sửa đề tài đăng ký!</h4>
                  </div>
                  <div class="modal-body" style="padding: 0px;"> 
                      
                     <textarea name ="chitiet" id="chitiet" rows="8" style="width: 100%; resize: none;" id ="editer" placeholder="Nhập đề tài........"><?php echo $chitiet; ?></textarea>     
                 </div>
                 <div class="modal-footer">
                  <p><button onclick = "javascript:Submit(<?php echo $_SESSION['idsv'];?>);">Gửi</button></p>
              </div>
              
              <script type="text/javascript">
                function Submit(id)
                {
                    var x,y;
                    y = id;
                    x = document.getElementById("chitiet").value;
                    

                    $.post('server.php', {giatrix: x, giatriy:y},
                        function(data){
                            $('#kt').html(data);
                        }, 'text');
                }

                
            </script></td> 
        </div>

    </div>
</div> 
<script>
    $(document).ready(function(){
        $("#Btn").click(function(){
            $("#Modal").modal();
        });
    });
</script>

<?php
if($chk == 1){
   
    ?>
    <table class="table bang" >
        <thead>
            <tr>
                <th class = 'qlyclick' style="border: none;" colspan="2">Đề tài bạn đang tham gia</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="them">Đề tài: </td>
                <td><?php echo $tendetai;?></td>
            </tr>
            <tr>
                <td class="them">Lĩnh vực: </td>
                <td><?php echo $row2['tenlinhvuc']; ?></td>
            </tr>
            <tr>
                <td class="them">Chi tiết: </td>
                <td><?php echo $chitiet;?><a id ="Btn"><i class="fa fa-edit" style="margin: 0px; font-size: 20px;"></i></a></td>
            </tr>
            <tr>
                <td class="them">Giáo viên: </td>
                <td><?php echo $row1['hoten']; ?></td>
            </tr>
            <tr>
                <td></td>
                <td> <a onclick="el(<?php echo $_SESSION['idsv'];?>)"><i class="fa fa-trash-o" style="font-size:24px" ></i></a></td>
            </tr>
        </tbody>
       
    </table>



    
    <?php
    echo "<span class = 'qlyclick'>Đăng ký của bạn đã được phê duyệt!</span>";
}else{
    ?>
    <table class="table bang" >
        <thead>
            <tr>
                <th class = 'qlyclick' style="border: none;" colspan="2">Danh mục đề tài tham gia</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="text-align: left; color: #88b77b;" class="qlyclick">Đề tài: </td>
                <td><?php echo $tendetai;?></td>
            </tr>
            <tr>
                <td style="text-align: left; color: #88b77b;" class="qlyclick">Lĩnh vực: </td>
                <td><?php echo $row2['tenlinhvuc']; ?></td>
            </tr>
            <tr>
                <td style="text-align: left; color: #88b77b;" class="qlyclick">Chi tiết: </td>
                <td><?php echo $chitiet;?><a id ="Btn"><i class="fa fa-edit" style="margin: 0px; font-size: 20px;"></i></a></td>
            </tr>
            <tr>
                <td style="text-align: left; color: #88b77b;" class="qlyclick">Giáo viên: </td>
                <td><?php echo $row1['hoten']; ?></td>
            </tr>
            <tr>
                <td></td>
                <td> <a onclick="el(<?php echo $_SESSION['idsv'];?>)"><i class="fa fa-trash-o" style="font-size:24px" ></i></a></td>
            </tr>
        </tbody>
    </table>
  
    <?php 
    echo "<span class = 'qlyclick' style='color:#88b77b;'>Đăng ký của bạn đang chờ phê duyệt!</span>";
}}
}
?>
<script type="text/javascript">
 function el(id){
        alert(id);
        $.ajax({
            type: "post",
            url:"server.php",
            data:{xoadki: id},
            success: function (data)
            {
                $('#kt').html(data);
            }

        });
    }
</script>
</div><!--  end-thamgia_set // In ra những danh mục đề tài tham gia ở trạng thái đã được xử lý hoặc chưa đc xử lý-->

<div id="detai_set"  style="margin-left: 50px;">
    
    <?php
    include('../../system/config/connect.php');

    $i = 0;
    if($_SESSION['user'] == "giaovien"){
        $sql = mysql_query("SELECT * FROM detai WHERE idgiaovien = '".$_SESSION['idsv']."'");
        if(mysql_num_rows($sql) == 0){echo "<span class = 'qlyclick'>Bạn không có chủ đề nghiên cứu nào!</span>";}
        while ($row = mysql_fetch_array($sql)) {
            $tendetai = $row['tendetai'];
            $idlinhvuc = $row['idlinhvuc'];
            $sql1 = mysql_query("SELECT * FROM linhvuc WHERE idlinhvuc = '".$idlinhvuc."'"); 
            $row1 = mysql_fetch_array($sql1);
            $tenlinhvuc = $row1['tenlinhvuc'];
            $i++;
            
            ?>
            <table class="table bang">
             <thead>
                <tr>
                    <th class = 'qlyclick' style="border: none;" colspan="2">Đề tài <?php echo $i; ?> :</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td  class="them">Tên đề tài : </td>
                    <td><?php echo $tendetai;?></td>
                </tr>
                <tr>
                    <td  class="them">Lĩnh vực: </td>
                    <td><?php echo $tenlinhvuc; ?></td>
                </tr>
            </tbody>
        </table>
        <?php
    }
}
?>
<form method="post">
    <table class="table bang">
     <thead>
        <tr>
            <th class = 'qlyclick' style="border: none;" colspan="2">Nhập chủ đề nghiên cứu</th>
        </tr>
    </thead>
    <tbody>
       
       <tr>
           <td style="color: #88b77b; font-size: 18px;font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif; ">Đề tài: </td>
           <td>
               <textarea style="resize: none;" name ="name" rows="8" cols="50" id ="editer" placeholder="Nhập đề tài........"></textarea>
           </td>
       </tr>
       <tr>
           <td style="color: #88b77b; font-size: 18px;font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif; ">Lĩnh vực: </td>
           <td>
               <select aria-lable="linhvuc" name="linhvuc" title="Lĩnh vực"  style="height: 40px;">
                   <option value="0" selected="1">Lĩnh vực</option>
                   <?php
                   include('../../system/config/connect.php');
                   $sql = mysql_query("SELECT * FROM linhvuc");
                   while ($row = mysql_fetch_array($sql)) {
                    $idlinhvuc = $row['idlinhvuc'];
                    $tenlinhvuc = $row['tenlinhvuc'];
                    
                    ?>
                    <option value=<?php echo $idlinhvuc;?>><?php echo $tenlinhvuc;?></option>  
                    <?php
                }
                ?>                                     
            </select>
        </td>
    </tr>
    <tr>
       <td></td>
       <td>
           <button type="submit" class="btn btn-default" name="detai" style="background: #88b77b;height: 40px;width: 80px; font-size: 18px; color: #ffffff;">Submit</button></td>
       </tr>
   </tbody>
</table> 
</form>                  
</div><!--  end-detai_set // In ra những đề tài của giáo viên và giáo viên nhập chủ đề --> 


</div>     

</div>
</div>
</section>
<!-- Xử lý các tab -->
<script type="text/javascript">
    function hide() {
        document.getElementById("hien_set").style.display = "none";
        document.getElementById("thamgia_set").style.display = "none";
        document.getElementById("detai_set").style.display = "none";
        document.getElementById("dangki_set").style.display = "none";
        document.getElementById("hien").className = "noselect";
        document.getElementById("thamgia").className = "noselect";
        document.getElementById("detai").className = "noselect";
        document.getElementById("dangki").className = "noselect";
    }  
    function show(paneid) {
        hide();
        document.getElementById(paneid+"_set").style.display = "";
        document.getElementById(paneid).className = "qlyclick";
    }   
    show("hien"); 
    function hidean(a) {
        document.getElementById(a).style.display = "none";
        document.getElementById(a).className = "noselect";
    } 
</script>
<?php
if($_SESSION['chksvbaove'] == 0){
    echo "<script>hidean('thamgia');</script>";
    echo "<script>hidean('thamgia_set');</script>";
    echo "<script>hidean('thamgia_an');</script>";
}
if($_SESSION['user'] != "giaovien"){
    echo "<script>hidean('detai');</script>";
    echo "<script>hidean('detai_set');</script>";
    echo "<script>hidean('dangki');</script>";
    echo "<script>hidean('dangki_set');</script>";
    echo "<script>hidean('dangki_an');</script>";
    echo "<script>hidean('detai_an');</script>";
}
?>
<?php
include('footer.php');
?>
</body>
</html>
