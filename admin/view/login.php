<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="../../public/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../../public/css/login.css">
	<script type="text/javascript" src="../../public/js/jquery-2.2.4.js"></script>
	<script type="text/javascript" src="../../public/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
</head>
<body>
	<header >	
		<div class="container" style ="background: #88b77b; width: 100%;height: 120px;">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<a class="navbar-brand" href="#"><img src="../../public/images/uet_logo.png"></a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse" style="margin-top:27px;">
						<ul class="nav navbar-nav">
							<li ><a href="#" style="font-size: 25px; color: #ffffff; font-weight: bold;text-shadow: 2px 2px 2px #cc0000;">ThisisMgr</a></li>
							<li><i class="fa fa-graduation-cap" style="font-size:40px;color: #cc0000; margin-top:10px;"></i></li>
						</ul>
						<form class="navbar-form navbar-left" role="search">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Search">
							</div>
							<button type="submit" class="btn btn-default" style="color: #000000;">Submit</button>
						</form>
						<ul class="nav navbar-nav navbar-right">
							<li style="color: #ffffff; font-size: 20px; margin-top: 10px;">You are not logged in</li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div>
			</nav>
		</div>
	</header>
	<section>
		<div class="container" style="width: 100%">
			<div class="row">
				<div class="col-md-4">
					<h3 style="color: #f60; padding: 20px; font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif;">Vui lòng đăng nhập</h3>
				</div>
				<!-- 	border: 2px solid red;  border-radius: 15px; -->
				<div class="col-md-4" style=" margin-top:130px;">
					<h2 class="log">Log in</h2>
					<form class="form-horizontal" action="" method="post" class="text-center" style="margin-top: 50px; margin-bottom: 20px;"">
						<div class="form-group" >
							<label for="inputEmail3" class="col-sm-2 control-label">Username: </label>
							<div class="col-sm-10">
								<input type="text" name ="user" class="form-control" id="input" placeholder="Username">
							</div>
						</div>
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">Password: </label>
							<div class="col-sm-10">
								<input type="password" name="pass" class="form-control" id="input" placeholder="Password">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<div class="checkbox">
									<label>
										<input type="checkbox"> Remember me
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-default" id = "sign" name="login" >Sign in</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-4"></div>
			</div>
		</div>
	</section>

</body>
</html>

