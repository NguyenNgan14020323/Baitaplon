<?php session_start();
if($_SESSION['loged'] == 0){
	header('location: ../controller/login.php');
} else{
//
	?>
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>ThesisMgr</title>
		<link rel="stylesheet" type="text/css" href="../../public/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../../public/css/mystyle.css">
		<script type="text/javascript" src="../../public/js/jquery-2.2.4.js"></script>
		<script type="text/javascript" src="../../public/js/bootstrap.min.js"></script>
		<!-- bs3-cdn -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	</head>
	<body>
		<?php
		include("head.php");
		?>
		<!-- Tham khảo TH6 Hàm cấu trúc cây -->
		<script type="text/javascript">
			function Folder_Toggle(myself) {
				if (myself.nextSibling.nextSibling.nextSibling.style.display == "") {
					myself.nextSibling.nextSibling.nextSibling.style.display = "none";
					myself.className = "fa fa-caret-right";
				} else {
					myself.nextSibling.nextSibling.nextSibling.style.display = "";
					myself.className = "fa fa-caret-down";
				}
			}

		</script>

		<p id="kt"></p>
		<input type="hidden" value="" id="an">
		<!-- Đăng kí đề tài cho sinh viên bảo vệ -->
		<div class="modal fade" id="Modal" role="dialog" style="margin-top: 50px;">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content" >
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-lable="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Đăng ký đề tài!</h4>
					</div>
					<div class="modal-body" style="padding: 0px;"> 
						<table class="table">
							<tbody>
								<?php
								$i = 10000;
								echo"<tr>
								<td id = ".($i + 1)."></td>

							</tr>
							<tr>
								<td id = ".($i + 2)."></td>

							</tr>";
							?>

							<tr style="padding: 0px;">

								<td  style="padding: 0px;">
									<textarea name ="chitiet" id="chitiet" rows="12" style="width: 100%; margin: 0px;resize: none; padding: 10px;" id ="editer" placeholder="Bạn muốn làm gì với đề tài này......"></textarea>

								</td>
							</tr>

						</tbody>
					</table>

				</div>
				<div class="modal-footer">
					<p type="submit" class="pull-left" data-dismiss="modal"><span class="fa fa-trash-o" style="font-size: 24px;"></p>
					<!-- 	<p><button onclick = "javascript:Submit(<?php //echo $iddetai;?>);">Gửi</button></p> -->
					<p><button type="button" class="btn btn-default" data-dismiss="modal" style="background: #222; color: #ffffff; width: 50px;" onclick="Close()">Gửi</button></p>
				</div>
			</div>

		</div>
	</div> 

	<section style="width: 100%;" id ="demo" >
		<div class="container" style="width: 100%;">
			<div class="row" style="width: 100%;">
				<!-- Danh mục lĩnh vực -->
				<div class="col-md-8"  id ="linhvuc_set" style="border: 1px solid #88b77b; margin: 50px;">
					<div class="tieude">
						<h2 style="text-shadow: 2px 2px 2px #cc0000;">Danh mục lĩnh vực</h2>	
					</div>
					<?php
					include('../../system/config/connect.php');
					$sql1 = mysql_query("SELECT * FROM linhvuc");

					while ($row1 = mysql_fetch_array($sql1)) {
						$idlinhvuc = $row1['idlinhvuc'];
						$tenlinhvuc = $row1['tenlinhvuc'];
						$index = 0;
						?>

						<div class="content-one" style="width: 100%">
							<div class="row">
								<div class="col-md-12">
									<ul class="tree">

										<li class="has-children"><i class="fa fa-caret-down" style="font-size:24px;" onclick="javascript:Folder_Toggle(this);" ></i><a class="qlyclick"><?php echo $tenlinhvuc; ?></a>
											<ul>
												<?php
												include('../../system/config/connect.php');
												$sql = mysql_query("SELECT * FROM detai WHERE idlinhvuc = '".$idlinhvuc."'");                     

												$_SESSION['dk'] = 1;
												$chk = 1;
												while ($row = mysql_fetch_array($sql)) {
													$idgiaovien = $row['idgiaovien'];
													$tendetai = $row['tendetai'];
													$iddetai = $row['iddetai'];
													$idsv = $row['idsv'];

													if($_SESSION['idsv'] == $idsv){
														$_SESSION['dk'] = 0;
														$chk = 0;
													}
													$index++;

													?>

													<?php
													$sql2 = mysql_query("SELECT * FROM giaovien g INNER JOIN detai d ON d.idgiaovien = g.idgiaovien WHERE d.iddetai = '".$iddetai."'");                     
													$row2 = mysql_fetch_array($sql2);
													$hoten2 = $row2['hoten'];
													$mail = $row2['email'];			
													?>

													<li  class="no-child">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th colspan="2" class="qlyclick" style="border: none;">Đề tài <?php echo $index; ?>: </th>
																</tr>
															</thead>
															<tbody>
																<tr>

																	<td><a id=<?php echo $iddetai;?> class="qlyclick" onclick="call(<?php echo $iddetai;?>)"><?php echo $tendetai;?></a></td>
																	<td><a style="margin: 15px;" class="qlyclick"><?php echo " - Giáo viên: " .$hoten2;?></a>
																		<br>
																		<a class="qlyclick"><?php echo  " - Email: " . $mail;?></a>
																	</td>
																</tr>
															</tbody>
														</table>


													</li>

													<?php
												}
												?>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<?php
					}
					?>
				</div>
				<!-- Danh mục giáo viên -->	
				<div class="col-md-8"  id ="giaovien_set" style="border: 1px solid #88b77b; margin: 50px; ">
					<div class="tieude">
						<h2 style="text-shadow: 2px 2px 2px #cc0000;">Danh mục giáo viên</h2>	
					</div>
					<p id="kt"></p>
					<?php
					include('../../system/config/connect.php');
					$sql1 = mysql_query("SELECT * FROM giaovien");

					while ($row1 = mysql_fetch_array($sql1)) {
						$idgiaovien = $row1['idgiaovien'];
						$hoten = $row1['hoten'];
						$index = 0;
						?>

						<div class="content-one" style="width: 100%">
							<div class="row">
								<div class="col-md-12">
									<ul class="tree">

										<li class="has-children"><i class="fa fa-caret-down" style="font-size:24px;" onclick="javascript:Folder_Toggle(this);" ></i><a class="qlyclick"><?php echo $hoten; ?></a>
											<ul>
												<li class="no-child" style="color: #f60; font-size: 18px; font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif; border: none;">Đề tài: </li>
												<?php
												include('../../system/config/connect.php');
												$sql = mysql_query("SELECT * FROM detai WHERE idgiaovien = '".$idgiaovien."'");                     


												while ($row = mysql_fetch_array($sql)) {
													$tendetai = $row['tendetai'];
													$iddetai = $row['iddetai'];
													$index++;
													$ngu = $iddetai + 100;
													?>
													<li class="no-child">
														<i class="fa fa-caret-right" style="font-size: 24px;" onclick="javascript:Folder_Toggle(this);"></i>

														<a id=<?php echo $ngu;?> class="qlyclick" onclick="ngu(<?php echo $ngu;?>)"><?php echo $tendetai;?></a>

													</li>

													<?php
												}
												?>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>

						<?php


					}
					?>

				</div>
				<script>
					function call(iddetai)
					{

						document.getElementById("Modal").style.display = "none";
						var data = "iddetai="+iddetai;
						document.getElementById("chitiet").value = "";
						$.post("server.php", data, function(data)
						{
							var j = 1, i = 0, Obj;
							document.getElementById((10000 + j)).innerHTML = "";
							document.getElementById((10001 + j)).innerHTML = "";
							Obj = JSON.parse(data);
							console.log(Obj);
							if(Obj[i].chksvbaove == 0){
								document.getElementById("Modal").style.display = "none";
							}else{
							if(Obj[i].chk == 1){
								document.getElementById("Modal").style.display = "none";

							}else{
								if(Obj[i].trung == 1){
									document.getElementById("Modal").style.display = "none";
									alert('De tai bi trung');

								}else{
									if(Obj[i].dangki == 1){
										document.getElementById("Modal").style.display = "none";
										alert('De tai da dang ki! Ban khong the dang ki!');

									}else{
										$(document).ready(function(){
											$("#"+iddetai).click(function(){
												$("#Modal").modal();
											});
										});

										document.getElementById((10000 + j)).innerHTML = "Tên đề tài: " + Obj[i].tendetai;
										document.getElementById((10001 + j)).innerHTML = "Giáo viên hướng dẫn: " +Obj[i].hoten;
										document.getElementById("an").value = Obj[0].iddetai;

									}
								}
							}}

						});

					}
					function ngu(id)
					{

						document.getElementById("Modal").style.display = "none";
						var iddetai = id - 100;
						var data = "iddetai="+iddetai;
						$.post("server.php", data, function(data)
						{
							var j = 1, i = 0, Obj;
							document.getElementById((10000 + j)).innerHTML = "";
							document.getElementById((10001 + j)).innerHTML = "";
							Obj = JSON.parse(data);
							console.log(Obj);
							if(Obj[i].chksvbaove == 0){
								document.getElementById("Modal").style.display = "none";
							}else{
							if(Obj[i].trung == 1){
								document.getElementById("Modal").style.display = "none";
								alert('De tai bi trung');

							}else{
								if(Obj[i].dangki == 1){
									document.getElementById("Modal").style.display = "none";
									alert('De tai da dang ki! Ban khong the dang ki!');

								}else{
									$(document).ready(function(){
										$("#"+id).click(function(){
											$("#Modal").modal();
										});
									});

									document.getElementById((10000 + j)).innerHTML = "Tên đề tài: " + Obj[i].tendetai;
									document.getElementById((10001 + j)).innerHTML = "Giáo viên hướng dẫn: " +Obj[i].hoten;
									document.getElementById("an").value = Obj[0].iddetai;

								}
							}}

						});

					}
					function Close(){
						var j = 1, i = 0;
						document.getElementById((10000 + j)).innerHTML = "";
						document.getElementById((10001 + j)).innerHTML = "";
						var id = parseInt(document.getElementById("an").value);
						x = document.getElementById("chitiet").value;
						$.ajax({
							type: "post",
							url:"server.php",
							data:{giatrix: x, giatriy:id},
							success: function (data)
							{
								$('#kt').html(data);
							}

						});
					}

				</script>
				<!-- Danh mục đơn vị -->
				<div class="col-md-8"  id ="donvi_set" style="border: 1px solid #88b77b; margin: 50px; ">
					<div class="tieude">
						<h2 style="text-shadow: 2px 2px 2px #cc0000;">Danh mục đơn vị</h2>	
					</div>
					<?php
					include('../../system/config/connect.php');
					$sql1 = mysql_query("SELECT * FROM khoa");

					while ($row1 = mysql_fetch_array($sql1)) {
						$idkhoa = $row1['idkhoa'];
						$tenkhoa = $row1['tenkhoa'];
						?>
						<ul class="tree">
							<li class="has-children"><i class="fa fa-caret-right" style="font-size: 24px;" onclick="javascript:Folder_Toggle(this);"></i><a class="qlyclick">Khoa <?php echo $tenkhoa; ?></a>
								<ul>
									<?php
									include('../../system/config/connect.php');
									$sql = mysql_query("SELECT * FROM linhvuc WHERE idkhoa = '".$idkhoa."'");                     
									while ($row = mysql_fetch_array($sql)) {
										$idlinhvuc = $row['idlinhvuc'];
										$tenlinhvuc = $row['tenlinhvuc'];
										?>
										<li class="has-children"><i class="fa fa-caret-right" style="font-size: 24px;" onclick="javascript:Folder_Toggle(this);"></i><a class="qlyclick"><?php echo $tenlinhvuc; ?></a>
											<ul>
												<?php
												$sql2 = mysql_query("SELECT * FROM bomon WHERE idlinhvuc = '".$idlinhvuc."'");
												while ($row3 = mysql_fetch_array($sql2)) {
													$tenbomon = $row3['tenbomon'];
													$idgiaovien = $row3['idgiaovien'];

													?>
													<li class="no-child"><a class="qlyclick"><?php echo $tenbomon; ?></a></li>
													<?php
												}
												?>
											</ul>
										</li>
										<?php
									}
									?>
								</ul>
							</li>
						</ul>
						<?php

					}
					?>
				</div>
				<!-- Thanh thực đơn -->
				<div class="col-md-3">
					<table class="table table-bordered" style="margin-top: 50px;" id="an">
						<thead>
							<tr>
								<th style="font-size: 18px; background: #88b77b; color: #ffffff;font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif;">Danh sách công việc: </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<a onclick="javascript:showcontent('linhvuc')" id="linhvuc"><i class="fa fa-arrow-circle-o-right" style="font-size:16px;color:#c0392b;"></i>Danh mục lĩnh vực</a>
								</td>
							</tr>
							<tr><td>
								<a onclick="javascript:showcontent('giaovien')" id="giaovien"><i class="fa fa-arrow-circle-o-right" style="font-size:16px;color:#c0392b;"></i>Danh mục giáo viên</a>
							</td>
						</tr>
						<tr><td>
							<a onclick="javascript:showcontent('donvi')" id="donvi"><i class="fa fa-arrow-circle-o-right" style="font-size:16px;color:#c0392b;"></i>Danh mục đơn vị</a>
						</td>
					</tr>
					<tr id="chanvai">
						<td> <div  id ="chanvai"> <a href="../../site/view/admin.php?id=0" id="qly" ><i class="fa fa-arrow-circle-o-right" style="font-size:16px;color:#c0392b;"></i>Dashboard</a></div></td>
					</tr>

				</tbody>
			</table>

		</div>
	</div>
</div>
<script>

	function hidework(){
		document.getElementById("chanvai").style.display = "none";
	}
	function showwork(){
		document.getElementById("chanvai").style.display = "";
	}
	function hidecontent(){
		document.getElementById("linhvuc_set").style.display = "none";
		document.getElementById("giaovien_set").style.display = "none";
		document.getElementById("donvi_set").style.display = "none";
		document.getElementById("linhvuc").className = "noselect";
		document.getElementById("giaovien").className = "noselect";
		document.getElementById("donvi").className = "noselect";
	}
	function showcontent(p){
		hidecontent();
		document.getElementById(p+"_set").style.display = "";
		document.getElementById(p).className = "qlyclick";
	}
	showcontent('linhvuc');

</script>
<?php
if($_SESSION['user'] == "admin"){
	echo "<script>showwork();</script>";
}else{
	echo "<script>hidework();</script>";
}
?>
</section>
<?php
include('footer.php');
?>
</body>
</html>
<?php
}
?>