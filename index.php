<!-- tham khảo giao diện course.uet.vnu.edu.vn -->
<?php session_start(); 
// $_SESSION['ngu'];
// if($_SESSION['ngu'] == 1){
// 	header('location: http://localhost/Baitaplon/admin/view/me.php');
// }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ThesisMgr</title>
	<link rel="stylesheet" type="text/css" href="public/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="public/css/mystyle.css">
	<script type="text/javascript" src="public/js/jquery-2.2.4.js"></script>
	<script type="text/javascript" src="public/js/bootstrap.min.js"></script>
	<!-- bs3-cdn -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
</head>
<body>
	<header>	
		<div class="container" style="width: 100%;">
			<div class="row" style ="background: #88b77b; width: 100%; height: 120px;">
				<nav class="navbar navbar-default" role="navigation">
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<a class="navbar-brand" href="#"><img src="public/images/uet_logo.png" style="ma"></a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse navbar-ex1-collapse" style="margin-top:27px;">
							<ul class="nav navbar-nav">
								<li ><a href="admin/view/me.php" style="font-size: 25px; color: #ffffff; font-weight: bold;
									text-shadow: 2px 2px 2px #cc0000;">ThesisMgr</a></li> 
									<li><i class="fa fa-graduation-cap" style="font-size:40px;color: #cc0000; margin-top:10px;"></i></li>
								</ul>
								<form class="navbar-form navbar-left" role="search">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Search">
									</div>
									<button type="submit" class="btn btn-default">Submit</button>
								</form>
								<ul class="nav navbar-nav navbar-right">
									<li  id = "login_set"><a href="admin/controller/login.php">Login</a></li>
								</ul>						
							</div><!-- /.navbar-collapse -->
						</div>
					</nav>
				</div>
				<div class="row" style="margin: 40px; margin-bottom: 30px;">
					<div class="text-right" id="diachi">
						<span style="font-size: 18px;
						color: #828282;
						display: inline-block;"><i class="fa fa-phone" style="color: #ff6c00;"></i>Call  US: (09) 8554 1627</span>
						<span style="font-size: 18px;
						color: #828282;
						display: inline-block;"><i class="fa fa-envelope-o" style="color: #ff6c00;"></i>Email: ngannt1710@gmail.com</span>
					</div>				
				</div>
				<div class="row">

					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">


							<div class="item active">
								<img src="public/images/slide-2.jpg" width="100%">
							</div>

							<div class="item">
								<img src="public/images/slide-3.jpg"  width="100%">

							</div>

							<div class="item">
								<img src="public/images/slide-1.jpg" width="100%">

							</div>

						</div>

						<!-- Left and right controls -->
						<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>

				</div>
			</div>
		</header>
		<script type="text/javascript">
			function Folder_Toggle(myself) {
				if (myself.nextSibling.nextSibling.nextSibling.style.display == "") {
					myself.nextSibling.nextSibling.nextSibling.style.display = "none";
					myself.className = "fa fa-caret-right";
				} else {
					myself.nextSibling.nextSibling.nextSibling.style.display = "";
					myself.className = "fa fa-caret-down";
				}
			}
		</script>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="tieude">
							<h2 style="text-shadow: 2px 2px 2px #cc0000;">Danh mục đơn vị</h2>	
						</div>
						<?php
						include('system/config/connect.php');
						$sql1 = mysql_query("SELECT * FROM khoa");

						while ($row1 = mysql_fetch_array($sql1)) {
							$idkhoa = $row1['idkhoa'];
							$tenkhoa = $row1['tenkhoa'];
							?>
							<ul class="tree">
								<li class="has-children"><i class="fa fa-caret-right" style="font-size: 24px;" onclick="javascript:Folder_Toggle(this);"></i><a class="qlyclick">Khoa <?php echo $tenkhoa; ?></a>
									<ul>
										<?php
										include('system/config/connect.php');
										$sql = mysql_query("SELECT * FROM linhvuc WHERE idkhoa = '".$idkhoa."'");                     
										while ($row = mysql_fetch_array($sql)) {
											$idlinhvuc = $row['idlinhvuc'];
											$tenlinhvuc = $row['tenlinhvuc'];
											?>
											<li class="has-children"><i class="fa fa-caret-right" style="font-size: 24px;" onclick="javascript:Folder_Toggle(this);"></i><a class="qlyclick"><?php echo $tenlinhvuc; ?></a>
												<ul>
													<?php
													$sql2 = mysql_query("SELECT * FROM bomon WHERE idlinhvuc = '".$idlinhvuc."'");
													while ($row3 = mysql_fetch_array($sql2)) {
														$tenbomon = $row3['tenbomon'];
														$idgiaovien = $row3['idgiaovien'];

														?>
														<li class="no-child"><i class="fa fa-caret-right" style="font-size: 24px;"></i><a class="qlyclick"><?php echo $tenbomon; ?></a></li>
														<?php
													}
													?>
												</ul>
											</li>
											<?php
										}
										?>
									</ul>
								</li>
							</ul>
							<?php

						}
						?>
					</div>
					<div class="col-md-4"></div>

				</div>
			</div>
		</section>
		<?php
		include('admin/view/footer.php');
		?>

	</body>

	</html>