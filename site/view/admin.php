<?php session_start(); 
//Kiểm tra nếu chưa dăng nhập thì đăng nhập
if($_SESSION['loged'] == 0){
	header('location:../../admin/controller/login.php');
}else {
	//kiểm tra nếu không phải là admin thì k đc vào trang này
	if($_SESSION['user'] != "admin"){
		echo("Trang này không tồn tại");
	}else{

		?>
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>ThesisMgr</title>
			<link rel="stylesheet" type="text/css" href="../../public/css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="../../public/css/mystyle.css">
			<script type="text/javascript" src="../../public/js/jquery-2.2.4.js"></script>
			<script type="text/javascript" src="../../public/js/bootstrap.min.js"></script>
			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			<!-- bs3-cdn -->
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
			<style type="text/css">
				.modal-header, h4, .close {
					background: #222;
					color: #ffffff;
					font-size: 18px;
				}
				.modal-footer {
					background-color: #f9f9f9;
				}
				.form{
					border: none;
					border-radius: 0px;
					margin-bottom: 0px;
					color: #222;
					background-color: white;
					font-size: 13px;
					margin: 0;
					outline: none;
					padding: 4px 2px 4px 2px;
					resize: none;
					-webkit-box-sizing: border-box;
					width: 100%;
					height: 50px;
					border-bottom: 1px solid #ccc;
				}
			</style>
		</head>
		<body>


			<?php
//Xử lý gửi mail người dùng
			include('../../system/config/connect.php');
			include('../../site/model/user.php');
			if($_SESSION['xoa'] == 1){
				echo "<script>alert('Xóa thành công!')</script>";
				$_SESSION['xoa'] = 0;
			}
			if($_SESSION['load'] == 1){
				echo "<script>alert('Import thành công!')</script>";
				$_SESSION['load'] = 0;
			}
			
			if(isset($_POST['ok'])){
				$mTo = $_POST['mTo'];
				$nTo = substr($mTo,0,7);
				$title = $_POST['title'];
				$content = $_POST['content'];
				$chk = sendmail($title,$content,$nTo,$mTo);
				if($chk == 1){
					echo "<script>alert('Gửi mail thành công')</script>";
				}else{
					echo "<script>alert('Lỗi!')</script>";
				}
			}
			?>
			<!-- Modal gửi email -->
			<div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content" style=" width: 510px">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-lable="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title"><span class="glyphicon glyphicon-envelope"></span>  Thư mới</h4>
						</div>
						<div class="modal-body" style="padding: 0px;">
							<form role="form" method="post" >
								<div class="form-group" style="margin-bottom: 0px;">
									<!-- <input type="text" class="form" id="mTo" name="mTo" placeholder="Người nhận"> -->
									<input type="email"  id="email" class="form" name="mTo" tabindex="2" placeholder="Người nhận" required>
									<input type="text" class="form" id="title" name="title" placeholder="Chủ đề">

									<textarea name ="content"  id ="editer" style="width: 100%; min-height: 280px; border: none; padding: 5px; resize: none;"></textarea>
									
								</div>


							</div>
							<div class="modal-footer">
								<p type="submit" class="pull-left" data-dismiss="modal"><span class="fa fa-trash-o" style="font-size: 24px;"></span></p>
								<p><button type="submit" name = "ok" id="btn" style="background: #222; color: #ffffff">Gửi</button></p>
							</div>
						</form>
					</div>

				</div>
			</div>
		</div>


		<script>
			$(document).ready(function(){
				$("#myBtn").click(function(){
					$("#myModal").modal();
				});
			});
		</script>
		<header >	
			<div class="container" style ="background: #88b77b; width: 100%;height: 120px;">
				<div class="row">
					<nav class="navbar navbar-default" role="navigation">
						<div class="container-fluid">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<a class="navbar-brand" href="#"><img src="../../public/images/uet_logo.png" style="ma"></a>
							</div>

							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse navbar-ex1-collapse" style="margin-top:27px;">
								<ul class="nav navbar-nav">
									<li ><a href="http://localhost/Baitaplon/admin/view/me.php" style="font-size: 25px; color: #ffffff; font-weight: bold;
										text-shadow: 2px 2px 2px #cc0000;">ThesisMgr</a></li>
										<li><i class="fa fa-graduation-cap" style="font-size:40px;color: #cc0000; margin-top:10px;"></i></li>
									</ul>
									<form class="navbar-form navbar-left" role="search">
										<div class="form-group">
											<input type="text" class="form-control" placeholder="Search">
										</div>
										<button type="submit" class="btn btn-default">Submit</button>
									</form>
								<ul class="nav navbar-nav navbar-right">							<!-- 
									<li><button type="button" onclick="loadDoc()" id="login_set">Change Content</button></li> -->
									<li class="dropdown" id="menu_set">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 18px; color: #ffffff;background: none;"><?php echo $_SESSION['hoten'];?><b class="caret"></b></a>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenuDivider">
											<li><a href="../../admin/view/me.php"><i class="fa fa-home"></i> ThesisMgr</a></li>
											<li  role="separator" class="divider"></li>
											<li><a href="../../admin/controller/profile.php"><i class="fa fa-user"></i> Profile</a></li>
											<li><a href="#" id="myBtn"><i class="fa fa-commenting-o"></i>Messages</a></li>
											<li><a href="" id="ngu" "><i class="fa fa-cog fa-spin" style="font-size:16px"></i> Preferences</a></li>

											<li  role="separator" class="divider"></li>
											<li><a href="../../admin/controller/logout.php"><i class="fa fa-unlock-alt"></i> Log out</a></li>
										</ul>
									</li>
								</ul>
							</div><!-- /.navbar-collapse -->
						</div>
					</nav>
				</div>
			</div>
			<div class="content">
				<div class="row" style="margin-top:25px;">
					<div >
						<a href="../../admin/view/me.php" style="font-size: 18px;
						color: #828282;
						display: inline-block;"><i class="fa fa-caret-right" style="color: #ff6c00;"></i>Home</a>
						<a href="" style="font-size: 18px;
						color: #828282;
						display: inline-block;"><i class="fa fa-caret-right" style="color: #ff6c00;"></i>Dashboard</a>
					</div>              
				</div>
			</div>
		</header>

		<div class="container" style="width: 100%">
			<div class="row">
				<div class="col-md-8" id="module4_set" style="border: 1px solid #88b77b; margin: 50px;">
					<div>
						<h3 style="color: #f60; padding: 20px; font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif;">Sửa đổi đề tài
						</h3>
					</div>
					
					<div class="text-center" id ="sua">
						
						<h3 class="qlyclick" style="font-size: 22px;">Thay đổi tên đề tài và thầy hướng dẫn </h3>
						<table class="table">
							<script type="text/javascript">
								function hide(){
									document.getElementById("sua").style.display = "none";
								}
								function show (){
									document.getElementById("sua").style.display = "";
								}
								
							</script>
							<?php
							include('../../system/config/connect.php');

							$id = $_GET['id'];
							if($id != 0){
								
								
								$sql = mysql_query("select * from detai where iddetai = '".$id."'");
								$row = mysql_fetch_array($sql);
								$tendetai = $row['tendetai'];
								$idgiaovien = $row['idgiaovien'];
								$idsv = $row['idsv'];
								$sql1 = mysql_query("select * from giaovien where idgiaovien = '".$idgiaovien."'");
								$row1 = mysql_fetch_array($sql1);
								$hotengv = $row1['hoten'];
								$danhdau = $row['danhdau'];
							}else{
								echo "<script>hide();</script>";
							}
							?>
							
							<tbody>
								<tr>
									<td style="text-align: left; color: #88b77b;" class="qlyclick">Tên đề tài</td>
									<td><input type="text" id="tendetai" value="<?php echo $tendetai; ?>" style ="height: 50px; width: 500px; padding: 15px;"></td>
								</tr>
								<tr>
									<td  style="text-align: left; color: #88b77b;" class="qlyclick">Giáo viên hướng dẫn</td>
									<td>
										<select aria-lable="giaovien" name="hotengv" id="hotengv" title="Lĩnh vực"  style ="height: 50px; width: 500px; padding: 15px;">>
											<option value=<?php echo $idgiaovien; ?> selected="1"><?php echo $hotengv;?></option>
											<?php
											include('../../system/config/connect.php');
											$sql2 = mysql_query("SELECT * FROM giaovien where idgiaovien NOT IN('".$idgiaovien."')");
											while ($row2= mysql_fetch_array($sql2)) {
												$idgiaovien = $row2['idgiaovien'];
												$hotengv = $row2['hoten'];
												
												?>
												<option value=<?php echo $idgiaovien;?>><?php echo $hotengv;?></option>  
												<?php

											}
											?>                                     
										</select>
									</td>
								</tr>
								<tr>
									<td style="text-align: left; color: #88b77b;" class="qlyclick">Rút đăng ký, xin thôi</td>
									<?php if($idsv == 0){?>
									<td class="qlyclick">Chưa có sinh viên đăng ký!</td>
									<?php }else{?>
									<td><input type="checkbox" name="rut" id="rut">
										<label class="qlyclick" >Rút đăng ký</label>
										<label style="margin-left: 50px;"><a id=<?php echo $idsv;?> class="qlyclick" onclick="nope(<?php echo $idsv;?>)"><span style="  font-size: 24px;" class="glyphicon glyphicon-envelope"></span>  Gửi mail</a></label>
									</td>
									<?php }?>
									<script type="text/javascript">
										function nope(idsv){
									 		var data = "idsv="+idsv;
									 		
											$.post("server.php", data, function(data){
												var j = 1, i = 0, Obj;
												Obj = JSON.parse(data);
												console.log(Obj);
												$(document).ready(function(){
													$("#"+idsv).click(function(){
														$("#Modal").modal();
													});
												});
												document.getElementById((10000 + j)).innerHTML = "Tới: " +Obj[i].hoten;		
												document.getElementById((10001 + j)).innerHTML = "Email: " + Obj[i].email;
												document.getElementById("an").value = Obj[0].idsv;
											});	
									 	}
									</script>
								</tr>
								<tr>
									<td style="text-align: left; color: #88b77b;" class="qlyclick">Trạng thái tiếp nhận hồ sơ</td>
									<?php if($danhdau == 0){?>
									<td><input type="radio" name="danhdau" value="0" id="chuadanhdau" checked="checked">
										<label class="qlyclick">Chưa nộp</label>
										<input type="radio" name="danhdau" value="1" id="danhdau">
										<label class="qlyclick">Đã nộp</label>
									</td>
									<?php }else{?>
									<td><input type="radio" name="danhdau" value="0" id="chuadanhdau">
										<label class="qlyclick">Chưa nộp</label>
										<input type="radio" name="danhdau" value="1" id="danhdau" checked="checked">
										<label class="qlyclick">Đã nộp</label>
									</td>
									<?php }?>
								</tr>
								<tr>
									<td></td>
									<td><button onclick = "javascript:Submit(<?php echo $id;?>);">Submit</button></td>
									<script type="text/javascript">
										
										function Submit(id)
										{
											var x,y;
											var t = 1;
											document.getElementById("sua").style.display = "none";
											x = document.getElementById("hotengv").value;
											y = document.getElementById("tendetai").value;
											if(document.getElementById("rut").checked){
												t = 0;
											}
											if(document.getElementById("chuadanhdau").checked){
											z = document.getElementById("chuadanhdau").value;}else{
												z = document.getElementById("danhdau").value;
											}								

				$.post('server.php', {giatrix: x, giatriy:y, giatriz:id, giatrit: z, giatrig: t},
					function(data){
						$('#kt').html(data);
					}, 'text');
			}		
		</script></td> 

	</tr>
</tbody>
</table>
</div>
<p id="kt"></p>
<table class="table table-bordered">
	<thead>
		<tr id="ngan">
			<style type="text/css">
				#ngan th{
					text-align: center;
				}
				#ngan{
					background:#88b77b;color: #ffffff;
					border: 1px solid #000000;

				}


			</style>
			<th>STT</th><th>Đề tài bảo vệ</th><th>Giáo viên hướng dẫn</th><th>MSSV</th><th>Họ tên</th><th>TT</th><th>Sửa</th><th>Xóa</th>
		</tr>
	</thead>
	<?php 


	databaseOutput(); ?>	
</table>
</div>
<p id="kt"></p>
<input type="hidden" value="" id="an">
<div class="modal fade" id="Modal" role="dialog" style="margin-top: 30px;">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content" style=" width: 510px">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-lable="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Gửi mail</h4>
                  </div>
                  <div class="modal-body" style="padding: 0px;"> 
                  <table class="table" style="margin-bottom: 0px;">
							<tbody>
								<?php
								$i = 10000;
								echo"<tr>
								<td id = ".($i + 1)."></td>

							</tr>
							<tr>
								<td id = ".($i + 2)."></td>

							</tr>";
							?>
							<tr style="padding: 0px;">

								<td  style="padding: 0px;">
									<input type="text" name="td" id="td" style="h padding: 10px; border: none; font-size: 14px;" class="form" placeholder="Tiêu đề">
								</td>
							</tr>
							<tr style="padding: 0px;">

								<td  style="padding: 0px;">
									<textarea name ="nd" id="nd" rows="12" style="width: 100%; margin: 0px;resize: none; padding: 10px;" placeholder="Nội dung....."></textarea>

								</td>
							</tr>

						</tbody>
					</table>    
                 </div>
                 <div class="modal-footer">
                  <p><button type="button" class="btn btn-default" data-dismiss="modal" style="background: #222; color: #ffffff; width: 50px;" onclick="ham();">Gửi</button></p>

              </div>
        </div>

    </div>
</div> 
 <script type="text/javascript">

    function ham()
    {
        var j = 1, i = 0;
        var idsv = parseInt(document.getElementById("an").value);
		 td = document.getElementById("td").value;
		 nd = document.getElementById("nd").value;
		$.ajax({
			type: "post",
			url:"server.php",
			data:{noidung: nd, tieude:td, idsv: idsv},
			success: function (data)
			{
				$('#kt').html(data);
			}

		});

    }
 


                
</script>

<div  id="qlysv_set" class="col-md-8" style="border: 1px solid #88b77b; margin: 50px; min-height: 500px;">
	<div>
		<h3 style="color: #f60; padding: 20px; font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif;">Quản lý sinh viên</h3>
	</div>
	<form enctype="multipart/form-data" action="../controller/upload_sinhvien.php" method="post"> 
		<input type="hidden" name="MAX_FILE_SIZE" value="2000000"/> 
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td style="width: 30%; color: #88b77b;" class="qlyclick">Danh sách sinh viên :</td> 
					<td style="width: 60%"><input type="file" name="file1" /></td> 
					<td><input type="submit" value="Upload"/></td> 
				</tr>
			</tbody>
		</table>
	</form> 
	<a href="dssv.php" class="qlyclick">Xem danh sách</a>
	<form enctype="multipart/form-data" action="../controller/upload_giaovien.php" method="post"> 
		<input type="hidden" name="MAX_FILE_SIZE" value="2000000"/> 
		<table class="table table-bordered">
			<tbody>
				<tr> 
					<td style="width: 30%; color: #88b77b;" class="qlyclick">Danh sách giáo viên :</td> 
					<td style="width: 60%"><input type="file" name="file2" /></td> 
					<td><input type="submit" value="Upload" /></td> 
				</tr>
			</tbody>
		</table>
	</form> 
	<a href="dsgv.php" class="qlyclick">Xem danh sách</a>	
	<form enctype="multipart/form-data" action="../controller/upload_svbaove.php" method="post"> 
		<input type="hidden" name="MAX_FILE_SIZE" value="2000000"/> 
		<table class="table table-bordered">
			<tbody>
				<tr> 
					<td style="width: 30%; color: #88b77b;" class="qlyclick">Danh sách sv đăng kí :</td> 
					<td style="width: 60%"><input type="file" name="file3" /></td> 
					<td><input type="submit" value="Upload" /></td> 
				</tr> 
			</tbody>
		</table>
	</form>

	<a class="qlyclick" onclick="hiennhap();">Nhập thông tin giáo viên </a>
	<div id ="nhap">
	<table class="table">
		<tbody>
			<tr>
				<td class="them">Idgiaovien</td>
				<td><input type="text"   name="idgv" id="idgv" style ="height: 50px; width: 300px; padding: 15px;"></td>
				<td></td>
			</tr>
			<tr>
				<td class="them">Idkhoa</td>
				<td>    <select aria-lable="khoa" name="idk" title="Khoa"  id="idk" style ="height: 50px; width: 300px; padding: 15px;">
                   <?php
                   include('../../system/config/connect.php');
                   $sql = mysql_query("SELECT * FROM khoa");
                   while ($row = mysql_fetch_array($sql)) {
                    $idkhoa = $row['idkhoa'];
                    $tenkhoa = $row['tenkhoa'];
                    
                    ?>
                    <option value=<?php echo $idkhoa;?>><?php echo $tenkhoa;?></option>  
                    <?php
                }
                ?>                                     
            </select></td>
            <td></td>
			</tr>
			<tr>
				<td class="them">Họ tên</td>
				<td><input type="text"   name="htgv" id="htgv" style ="height: 50px; width: 300px; padding: 15px;"></td>
				<td></td>
			</tr>
			<tr>
				<td class="them">Email: </td>
				<td><input type="text"  onblur ="javascript:xuly(this);" name="email" id="em" style ="height: 50px; width: 300px; padding: 15px;"></td>
				<td><p id="loi_email" class="qlyclick" ></p></td>
			</tr>
			
			<tr>
				<td></td>
				<td><button type="button" class="btn btn-default"  style="background: #222; color: #ffffff; width: 80px;" onclick="guidi()">Submit</button></td>

			</tr>
		</tbody>
	</table>
	</div>
	<script type="text/javascript">
	function xuly(){
		document.getElementById("loi_email").innerHTML ="";
		if(document.getElementById("em").value == ""){
			document.getElementById("loi_email").innerHTML ="Bạn chưa nhập email";
		}

	}
		function annhap(){
			document.getElementById("nhap").style.display = "none";
		}
		annhap();
		function hiennhap(){
			if(document.getElementById("nhap").style.display == "none"){
			document.getElementById("nhap").style.display = "";
		}else{
			document.getElementById("nhap").style.display = "none";
		}
		}
			function guidi(){
			
				idgiaovien = document.getElementById("idgv").value;
				khoa = document.getElementById("idk").value;
				em = document.getElementById("em").value;
				hoten = document.getElementById("htgv").value;
				$.ajax({
					type: "post",
					url:"server.php",
					data:{idgv: idgiaovien, kh:khoa, email:em, ht: hoten},
					success: function (data)
					{
						$('#kt').html(data);
					}

				});
			}
	</script>
		<p class="qlyclick">Đóng đăng ký: </p>
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td style="width: 60%; color: #88b77b;" class="qlyclick">Đóng đăng ký đề tài</td> 
					<td><button onclick = "javascript:huy();">Đóng</button></td>
				</tr>
			</tbody>
		</table>
	
	<script type="text/javascript">
	function huy(){
		$.post('server.php', {khoadangki: "sinhvien"},
			function(data){
				$('#kt').html(data);
			}, 'text');

	}
	</script>
</div>
<div id="guimail_set" class="col-md-8" style="border: 1px solid #88b77b; margin: 50px;">
	<div>
		<h3 style="color: #f60; padding: 20px; font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif;">Gửi email thông báo
		</h3>
	</div>
	<table class="table bang">
			<thead>
				<tr>
					<th class = 'qlyclick' style="border: none;" colspan="2">Thông báo mật khẩu kích hoạt tài khoản</th>
				</tr>
			</thead>
		</table>
	<form action="../controller/sendmail.php" method="post">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td style="width: 70%; color: #88b77b;" class="qlyclick">Gửi mail tới giáo viên: </td>
					<td><input type="submit" name="submit1" value="Send"></td>
				</tr>
			</tbody>
		</table>						
	</form>
	<form action="../controller/sendmail.php" method="post">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td style="width: 70%; color: #88b77b;" class="qlyclick">Gửi mail tới sinh viên: </td>
					<td><input type="submit" name="submit2" value="Send" ></td>
				</tr>
			</tbody>
		</table>						
	</form>
		<table class="table bang">
			<thead>
				<tr>
					<th class = 'qlyclick' style="border: none;" colspan="2">Thông báo tới sinh viên đủ điều kiện tham gia bảo vệ</th>
				</tr>
			</thead>
		</table>
	<form action="../controller/sendmail.php" method="post">
		<table class="table table-bordered">
			<tbody>
				<tr>
					<td style="width: 70%; color: #88b77b;" class="qlyclick">Gửi mail tới sinh viên bảo vệ: </td>
					<td><input type="submit" name="submit3" value="Send"></td>
				</tr>
			</tbody>
		</table>						
	</form>
	<form action="../controller/sendmail.php" method="post">
		<table class="table bang">
			<thead>
				<tr>
					<th class = 'qlyclick' style="border: none;" colspan="2">Thông báo</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="color: #88b77b; font-size: 18px;font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif; ">Gửi tới: </td>
					<td>
						<select name="guitoi" style="height: 40px;">
							<option value="sinhvienbaove">Sinh viên bảo vệ</option>
							<option value="sinhvien">Sinh viên</option>
							<option value="giaovien">Giáo viên</option>
						</select>
					</td>
				</tr>
				<tr>
					<tr>
						<td style="color: #88b77b; font-size: 18px;font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif; ">Nội dung: </td>
						<td>
							<div><input type="text" name="title" placeholder="Tiêu đề" style="width: 605px; height: 50px"></div>
							<textarea style="resize: none;" name ="content" rows="8" cols="83" id ="editer" placeholder="Nhập nội dung........"></textarea>
							<script type="text/javascript">CKEDITOR.replace( 'editer');</script>
						</td>
					</tr>

					<td></td>
					<td><button type="submit" name = "submit4">Gửi</button></td>
				</tr>
			</tbody>
		</table>
	</form>

</div>	
<p id="kt"></p>
<input type="hidden" value="" id="nganngaongo">
<input type="hidden" value="" id="setgiatri">
<div id="phancong_set" class="col-md-8" style="border: 1px solid #88b77b; margin: 50px;">
	<div>
		<h3 style="color: #f60; padding: 20px; font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif;">Phân công phản biện
		</h3>
	</div>
	<div>

	<table class="table table-bordered">
			<tbody>
				<tr>
				<form method="post" action="server.php">
					<td style="width: 70%; color: #88b77b;" class="qlyclick">Xuất Đề nghị hội đồng bảo vệ </td>
					<td><input type="submit" name="nganxinh" value="Dowload" ></td>
					</form>
				</tr>
			</tbody>
	</table>
	</div>
	<div class="content_phancong" style="width: 100%">
		<div class="row">
			<div class="col-md-12">
				<?php
					$query = mysql_query("SELECT * FROM detai where NOT idsv= 0 and chk= 1");
					if(mysql_num_rows($query) == 0){
						echo "<span class='qlyclick'>Không có đề tài nào</span>";
					}else{
						while ($row = mysql_fetch_array($query)) {
							$iddetai = $row['iddetai'];
							$tendetai = $row['tendetai'];
							$idsv = $row['idsv'];
							$idgiaovien = $row['idgiaovien'];
							$s = mysql_query("SELECT * FROM sinhvien where idsv = '".$idsv."'");
							$s1 = mysql_fetch_array($s);
							$tensinhvien = $s1['hoten'];
							$s2 = mysql_query("SELECT * FROM giaovien where idgiaovien = '".$idgiaovien."'");
							$s3 = mysql_fetch_array($s2);
							$hoten = $s3['hoten'];
				?>
							<ul class="tree">
								<li class="has-children"><i class="fa fa-caret-right" style="font-size:24px;"></i><span class="qlyclick">Đề tài: </span><a class="qlyclick"><?php echo $tendetai; ?></a>
									<ul>
									<li><i class="fa fa-caret-right" style="font-size:24px;"></i><span class="qlyclick">Sinh viên bảo vệ: <span class="them"><?php echo $tensinhvien;?></span></span></li>
									<li><i class="fa fa-caret-right" style="font-size:24px;"></i><span class="qlyclick">Giáo viên hướng dẫn: <span class="them"><?php echo $hoten;?></span></span></li>
									<li><i class="fa fa-caret-right" style="font-size:24px;"></i><span class="qlyclick">Hội đồng phản biện: </span>
									<?php
										$sql = mysql_query("SELECT * FROM phancong where iddetai = '$iddetai'");
										while ($row1 = mysql_fetch_array($sql)) {
												$tengiaovien = $row1['tengiaovien'];
												$id = $row1['id'];
									?>
									<ul>
										<li class="them">- <?php echo $tengiaovien; ?><span onclick="del(<?php echo $id;?> );"><i class="fa fa-user-times" style="font-size:24px" ></i></span></li>
									</ul>
									
									</li>

									<?php
										}
									?>
									<li><p><a onclick="goi(<?php echo $iddetai;?>);"><i class="fa fa-user-plus" style="font-size:24px"></i>Add</a></p></li>
									<li>
										<div id=<?php echo "nganngu".$iddetai;?>>
											<label>Họ tên giáo viên: </label>
											<p><input type="text" id = <?php echo "giaovien".$iddetai;?>  onblur="javascript: this.value = ChuanhoaTen(this.value);"  onkeyup="my(<?php echo $iddetai;?>);" style="height: 40px; min-width: 500px" name="giaovien">
											<button type="button" class="btn btn-default" style="background: #222; color: #ffffff; width: 50px;" onclick="gui()">Add</button>
											</p>											
											<ul id=<?php echo "hienthi".$iddetai;?> style="height: 200px;"></ul>
											<p></p>
										</div>
									</li>
									</ul>
									
									
								</li>
							</ul>
				<?php
						}
					}
				?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function ChuanhoaTen(ten) {
		dname = ten;
		ss = dname.split(' ');
		dname = "";
		for (i = 0; i < ss.length; i++)
			if (ss[i].length > 0) {
				if (dname.length > 0) dname = dname + " ";
				dname = dname + ss[i].substring(0, 1).toUpperCase();
				dname = dname + ss[i].substring(1).toLowerCase();
			}
		return dname;
	}
	function angui(){
		document.getElementById("nganngu4").style.display = "none";
		document.getElementById("nganngu7").style.display = "none";
		document.getElementById("nganngu9").style.display = "none";
		document.getElementById("nganngu15").style.display = "none";
	}
	angui();
	function del(id){
		
		$.ajax({
			type: "post",
			url:"server.php",
			data:{del: id},
			success: function (data)
			{
				$('#kt').html(data);
			}

		});
	}
	function my(id){
		document.getElementById("hienthi" + id).innerHTML = "";
		var ele = document.getElementById("hienthi" + id).innerHTML;
		var x = document.getElementById("giaovien"+id).value;	
		var ele ='';
		var data = "hienthi="+x;
		$.post("server.php", data, function(data)
		{
			var j = 1, i = 0, Obj;
			Obj = JSON.parse(data);
			leng = Object.keys(Obj).length;
			
			console.log(Obj);
			
			for(i = 0; i <leng; i++){
			 	ele += '<li>'+ Obj[i].hoten + '</li>';		
			}
			if(ele == ""){
				document.getElementById("hienthi" + id).innerHTML = "Không tồn tại giảng viên này!";
			}else{
				document.getElementById("hienthi" + id).innerHTML += ele;
			}
		});

	}

	
	function goi(iddetai){
		document.getElementById("nganngaongo").value = iddetai;
		document.getElementById("nganngu"+iddetai).style.display = "";
	}
	function gui(){
		var id = parseInt(document.getElementById("nganngaongo").value);
		x = document.getElementById("giaovien" + id).value;

			$.ajax({
				type: "post",
				url:"server.php",
				data:{idphancong: id, tengiaovien:x},
				success: function (data)
				{
					$('#kt').html(data);
				}

			});
	}
</script>
<div class="col-md-3">
	<table class="table table-bordered" style="margin-top: 50px;" id="an">
		<thead>
			<tr>
				<th style="font-size: 18px; background: #88b77b; color: #ffffff;font-weight: normal; font-family: Century Gothic,Arial,Helvetica,sans-serif;">Danh sách công việc: </th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<a id="qlysv"  onclick="javascript:showcontent('qlysv');"><i class="fa fa-arrow-circle-o-right" style="font-size:16px;color:#c0392b;"></i>Quản lý sinh viên</a>
				</td>
			</tr>
			<tr>
				<td>

					<a id="guimail"  onclick="javascript:showcontent('guimail');"><i class="fa fa-arrow-circle-o-right" style="font-size: 16px; color: #c0392b"></i>Gửi mail thông báo</a>

				</td>								
			</tr>
			<tr>
				<td>

					<a id="module4"  onclick="javascript:showcontent('module4');"><i class="fa fa-arrow-circle-o-right" style="font-size: 16px; color: #c0392b"></i>Sửa đổi đề tài</a>

				</td>								
			</tr>	
			<tr>
				<td>

					<a id="phancong"  onclick="javascript:showcontent('phancong');"><i class="fa fa-arrow-circle-o-right" style="font-size: 16px; color: #c0392b"></i>Phân công phản biện</a>

				</td>								
			</tr>		
			<tr>
				<td> <a class="noselect" href="../view/export.php"><i class="fa fa-arrow-circle-o-right" style="font-size:16px; color:#c0392b;"></i>Xuất công văn</a></td>
			</tr>
			

			</tbody>
		</table>

	</div>

</div>
<script>
	function hidecontent(){
		document.getElementById("qlysv_set").style.display = "none";
		document.getElementById("guimail_set").style.display = "none";
		document.getElementById("module4_set").style.display = "none";
		document.getElementById("phancong_set").style.display = "none";
		document.getElementById("qlysv").className = "noselect";
		document.getElementById("guimail").className = "noselect";
		document.getElementById("module4").className = "noselect";
		document.getElementById("phancong").className = "noselect";
	}
	function showcontent(p){
		hidecontent();
		document.getElementById(p+"_set").style.display = "";
		document.getElementById(p).className = "qlyclick";
	}
	showcontent('module4');


</script>
<script type="text/javascript">
	// function getConfirm(){
	// 	var retVal = confirm("Do you want to continue ?");
	// 	if( retVal == true ){
	// 		document.write ("User wants to continue!");
	// 		return true;
	// 	}
	// 	else{
	// 		document.write ("User does not want to continue!");
	// 		return false;
	// 	}
	// }
         //-->
     </script>
 </div>
 <?php
 if($_SESSION['chkmail'] == 1){
 	echo "<script>alert('Gửi mail thành công');</script>";
 	$_SESSION['chkmail'] =0;
 }
 
 ?>
 <?php
 include('../../admin/view/footer.php');
 ?>
</body>
</html>
<?php
}
}
?>